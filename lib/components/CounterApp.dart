import 'package:flutter/material.dart';
import 'package:learn_flutter/components/RoutePage.dart';

class CounterApp extends StatefulWidget {
  const CounterApp({Key? key}) : super(key: key);

  @override
  _CounterAppState createState() => _CounterAppState();
}

class _CounterAppState extends State<CounterApp> {
  int counter = 0;

  void _incrementCounter() {
    setState(() {
      counter += 1;
    });
  }

  void _decrementCounter() {
    setState(() {
      if (counter > 0) {
        counter -= 1;
      } else {
        counter = 0;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("Counter App"),
        backgroundColor: Colors.orange,
      ),
      body: Center(
          child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              counter.toString(),
              style: TextStyle(fontSize: 30 + counter.toDouble()),
            ),
            CounterTools(
                counter: counter,
                increment: _incrementCounter,
                decrement: _decrementCounter)
          ],
        ),
      )),
    ));
  }
}

class CounterTools extends StatefulWidget {
  final counter;
  final increment;
  final decrement;

  void multipleDescrement(int n) {
    for (int i = 0; i < n; i++) {
      decrement();
    }
  }

  void showAlertDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Counter telah lebih dari 10"),
            actions: [
              FlatButton(
                  onPressed: () {
                    multipleDescrement(11);
                    Navigator.of(context).pop();
                  },
                  child: Text("Reset")),
              FlatButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                  child: Text("Close"))
            ],
          );
        });
  }

  const CounterTools(
      {Key? key,
      this.counter = 0,
      required this.increment,
      required this.decrement})
      : super(key: key);

  @override
  _CounterToolsState createState() => _CounterToolsState();
}

class _CounterToolsState extends State<CounterTools> {
  @override
  Widget build(BuildContext context) {
    List<Widget> buttonWidget = [
      Container(
          child: ElevatedButton(
              child: Icon(Icons.add),
              onPressed: () {
                if (widget.counter == 10) {
                  widget.showAlertDialog(context);
                }

                if (widget.counter > 20) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RoutePage(
                              counter: widget.counter,
                              increment: widget.increment,
                              decrement: widget.decrement)));
                } else {
                  widget.increment();
                }
              }),
          margin: EdgeInsets.only(right: 15)),
      ElevatedButton(child: Icon(Icons.remove), onPressed: widget.decrement),
    ];

    return Container(
        child: Column(children: <Widget>[
      Row(children: buttonWidget, mainAxisAlignment: MainAxisAlignment.center),
      Text(
        "Past : " + (widget.counter - 1).toString(),
        style: TextStyle(fontSize: 20),
      )
    ]));
  }
}

import 'package:flutter/material.dart';
import 'package:learn_flutter/components/LayoutGridExtent.dart';

class TrainingNavigation extends StatelessWidget {
  const TrainingNavigation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: MainScreen());
  }
}

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Tugas 6")),
      ),
      body: Container(
          child: GestureDetector(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return SecondScreen();
          }));
        },
        child: Hero(
            tag: "heroImage",
            child: Image.network("https://picsum.photos/200")),
      )),
    );
  }
}

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Tugas 6")),
      ),
      body: Container(
          child: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Center(
          child: Hero(
            tag: "heroImage",
            child: Image.network("https://picsum.photos/200"),
          ),
        ),
      )),
    );
  }
}

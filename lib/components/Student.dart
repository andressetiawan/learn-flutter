class Student {
  int id;
  String name;
  int age;

  Student({required this.id, required this.name, required this.age});
}

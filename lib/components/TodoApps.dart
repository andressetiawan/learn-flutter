import 'package:flutter/material.dart';

class Todo {
  String title;
  String desc;

  Todo({required this.title, required this.desc});
}

class ScreenArguments {
  String text;
  ScreenArguments({required this.text});
}

class TodoApps extends StatefulWidget {
  const TodoApps({Key? key}) : super(key: key);

  @override
  _TodoAppsState createState() => _TodoAppsState();
}

class _TodoAppsState extends State<TodoApps> {
  int selectedIndex = 0;
  void selectedTodos(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Todo> todos = List.generate(
        20,
        (index) => Todo(
            title: "Todo #${index + 1}",
            desc:
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."));

    return MaterialApp(
        initialRoute: MainScreen.routeName,
        routes: {
          MainScreen.routeName: (context) =>
              MainScreen(todos: todos, selectedFunc: selectedTodos),
          DetailScreen.routeName: (context) =>
              DetailScreen(todo: todos[selectedIndex])
        },
        home: MainScreen(
          todos: todos,
          selectedFunc: selectedTodos,
        ));
  }
}

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key, required this.todos, required this.selectedFunc})
      : super(key: key);

  final List<Todo> todos;
  final selectedFunc;

  static final String routeName = "/main";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Todo Apps"),
          backgroundColor: Colors.pink,
        ),
        body: ListView.builder(
            padding: EdgeInsets.symmetric(horizontal: 24),
            itemCount: todos.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () => showTodosDesc(context, todos[index], index),
                child: Container(
                  margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Row(
                    children: [
                      Container(
                        width: 40,
                        height: 40,
                        margin: EdgeInsets.only(right: 16),
                        child: CircleAvatar(
                            backgroundImage:
                                NetworkImage("https://picsum.photos/200")),
                      ),
                      Text(
                        todos[index].title,
                        style: TextStyle(fontSize: 16),
                      ),
                    ],
                  ),
                ),
              );
            }));
  }

  GestureTapCallback? showTodosDesc(
      BuildContext context, Todo todo, int index) {
    // Navigator.push(context, MaterialPageRoute(builder: (context) {
    //   return DetailScreen(todo: todo);
    // }));
    selectedFunc(index);
    Navigator.pushNamed(context, DetailScreen.routeName,
        arguments: ScreenArguments(text: "Current todo : " + todo.title));
  }
}

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key, required this.todo}) : super(key: key);
  final Todo todo;
  static final String routeName = "/detail";

  @override
  Widget build(BuildContext context) {
    final ScreenArguments args =
        ModalRoute.of(context)!.settings.arguments as ScreenArguments;

    return Scaffold(
        appBar: AppBar(
          title: Text(todo.title),
          backgroundColor: Colors.redAccent,
        ),
        body: Container(
          margin: EdgeInsets.only(top: 20),
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(children: <Widget>[
            Container(
                margin: EdgeInsets.only(bottom: 8),
                child: Text(
                  todo.title,
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                )),
            Text(todo.desc),
            Container(
              margin: EdgeInsets.only(top: 24),
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color?>(Colors.redAccent),
                  ),
                  onPressed: () {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(args.text),
                      action: SnackBarAction(
                        label: "Close",
                        onPressed: () {},
                      ),
                    ));
                  },
                  child: Text("Show Argument")),
            )
          ]),
        ));
  }
}

import 'package:flutter/material.dart';

class ColumnRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Column and Row!"),
          backgroundColor: Colors.orange,
        ),
        body: Column(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(top: 24),
                child: Text(
                  "These are pictures",
                  style: TextStyle(
                      fontFamily: 'poppins',
                      fontSize: 18,
                      color: Colors.orange,
                      fontWeight: FontWeight.w500),
                )),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                  border: Border.all(width: 2, color: Colors.black12)),
              margin: EdgeInsets.only(left: 20, right: 20, top: 20),
              height: 100,
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/Avatar1.jpg"),
                            fit: BoxFit.fill),
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        border: Border.all(color: Colors.black12, width: 2)),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/Avatar2.jpg"),
                            fit: BoxFit.fill),
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        border: Border.all(color: Colors.black12, width: 2)),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

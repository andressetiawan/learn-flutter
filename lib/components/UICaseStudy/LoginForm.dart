import 'package:flutter/material.dart';
import 'package:learn_flutter/components/UICaseStudy/User.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MainScreen(),
    );
  }
}

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _isPasswordShow = true;
  User user = new User();

  void tooglePassword() {
    setState(() {
      _isPasswordShow = !_isPasswordShow;
    });
  }

  void changeUserData(String value, String type) {
    setState(() =>
        (type == "username") ? user.username = value : user.password = value);
  }

  Container tableDataWidget(String value, BorderRadius borderRadius) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey),
          borderRadius: borderRadius),
      padding: EdgeInsets.all(20),
      alignment: Alignment.center,
      child: Text(value,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.home),
        title: Text("Login"),
      ),
      body: Form(
          key: _formKey,
          child: Container(
              padding: EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    onChanged: (value) => changeUserData(value, "username"),
                    decoration: InputDecoration(
                        icon: Icon(Icons.people),
                        hintText: "Username",
                        border: OutlineInputBorder()),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: TextFormField(
                      onChanged: (value) => changeUserData(value, "password"),
                      obscureText: _isPasswordShow,
                      decoration: InputDecoration(
                          icon: Icon(Icons.password),
                          suffixIcon: IconButton(
                            icon: Icon((_isPasswordShow
                                ? Icons.remove_red_eye_outlined
                                : Icons.remove_red_eye)),
                            onPressed: tooglePassword,
                          ),
                          hintText: "Password",
                          border: OutlineInputBorder()),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 32),
                    child: Table(
                      columnWidths: const <int, TableColumnWidth>{
                        0: FlexColumnWidth(1),
                        1: FlexColumnWidth(1.25),
                        2: FlexColumnWidth(1.25)
                      },
                      children: [
                        TableRow(children: [
                          tableDataWidget("User ID",
                              BorderRadius.only(topLeft: Radius.circular(16))),
                          tableDataWidget("Username", BorderRadius.zero),
                          tableDataWidget("Password",
                              BorderRadius.only(topRight: Radius.circular(16))),
                        ]),
                        TableRow(children: [
                          tableDataWidget("1", BorderRadius.zero),
                          tableDataWidget(
                              user.username.toString(), BorderRadius.zero),
                          tableDataWidget(
                              user.password.toString(), BorderRadius.zero),
                        ]),
                      ],
                    ),
                  )
                ],
              ))),
    );
  }
}

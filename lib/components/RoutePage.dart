import 'package:flutter/material.dart';

class RoutePage extends StatefulWidget {
  final counter;
  final increment;
  final decrement;

  Future<TextEditingController> showTextAlert(BuildContext context) async {
    TextEditingController _textFieldController = new TextEditingController();

    var result = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Nama kamu adalah "),
            content: TextField(
              controller: _textFieldController,
              decoration: InputDecoration(hintText: "Input your name"),
            ),
            actions: <Widget>[
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true)
                        .pop(_textFieldController);
                  },
                  child: Text("Confirm")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true)
                        .pop(_textFieldController);
                  },
                  child: Text("Cancel"))
            ],
          );
        });

    return result as TextEditingController;
  }

  Future<EnumAction> showOptinAlert(BuildContext context) async {
    var result = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text("Select actions : "),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, EnumAction.minus10);
                },
                child: const Text("Minus 10 counter"),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, EnumAction.minus5);
                },
                child: const Text("Minus 5 counter"),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, EnumAction.plus10);
                },
                child: const Text("Plus 10 counter"),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, EnumAction.plus5);
                },
                child: const Text("Plus 5 counter"),
              )
            ],
          );
        });

    return result as EnumAction;
  }

  const RoutePage(
      {Key? key,
      this.counter = 0,
      required this.increment,
      required this.decrement})
      : super(key: key);

  @override
  _RoutePageState createState() => _RoutePageState();
}

class _RoutePageState extends State<RoutePage> {
  List<Widget> text = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Counter App Route #2")),
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              Text(
                widget.counter.toString(),
                style: TextStyle(fontSize: (30.0 + widget.counter)),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 20),
                    child: ElevatedButton(
                        onPressed: widget.increment, child: Icon(Icons.add)),
                  ),
                  ElevatedButton(
                      onPressed: () {
                        if (widget.counter < 20) {
                          Navigator.pop(context);
                        } else {
                          widget.decrement();
                        }
                      },
                      child: Icon(Icons.remove)),
                ],
              ),
              ElevatedButton(
                  onPressed: () async {
                    EnumAction result = await widget.showOptinAlert(context);
                    print(result);
                  },
                  child: Text("Special button")),
              ElevatedButton(
                  onPressed: () async {
                    TextEditingController result =
                        await widget.showTextAlert(context);
                    print(result.text);
                  },
                  child: Text("Input button")),
            ])));
  }
}

enum EnumAction { plus5, plus10, minus5, minus10 }

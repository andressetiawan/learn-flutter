import 'package:flutter/material.dart';
import 'package:learn_flutter/components/Student.dart';

class ListViewPage extends StatefulWidget {
  const ListViewPage({Key? key}) : super(key: key);

  @override
  _ListViewPageState createState() => _ListViewPageState();
}

class _ListViewPageState extends State<ListViewPage> {
  Student std1 = new Student(id: 1910101028, name: "Andres", age: 21);
  Student std2 = new Student(id: 1910101031, name: "Afwa", age: 24);
  Student std3 = new Student(id: 1910101010, name: "Darryl", age: 22);
  List<Student> students = [];

  void initState() {
    super.initState();
    students = <Student>[std1, std2, std3];
  }

  @override
  Widget build(BuildContext context) {
    TextStyle TextStyleWidget() {
      return TextStyle(
          color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold);
    }

    return MaterialApp(
      home: Scaffold(
          body: ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 48),
              itemCount: students.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.blue,
                  ),
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                  margin: EdgeInsets.only(top: 24),
                  height: 100,
                  child: Column(
                    children: <Widget>[
                      Text(
                        "NIM : ${students[index].id}",
                        style: TextStyleWidget(),
                      ),
                      Text(
                        "Name : ${students[index].name}",
                        style: TextStyleWidget(),
                      ),
                      Text(
                        "Age : ${students[index].age}",
                        style: TextStyleWidget(),
                      ),
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                );
              })),
    );
  }
}

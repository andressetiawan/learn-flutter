import 'package:flutter/material.dart';

class LayoutGridExtent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Grid view extends!"),
        ),
        body: GridView.extent(
          maxCrossAxisExtent: 300,
          children: List.generate(10, (index) {
            return Center(
                child: Container(
              margin: EdgeInsets.all(2.5),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 0.5,
                        blurRadius: 5,
                        offset: Offset(1, 2))
                  ]),
              child: Column(children: <Widget>[
                Image.asset(
                  'assets/images/Avatar2.jpg',
                  height: 135,
                ),
                Text("Product $index")
              ]),
            ));
          }),
        ),
      ),
    );
  }
}

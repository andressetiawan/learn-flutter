import 'package:flutter/material.dart';

class ProfileForm extends StatelessWidget {
  const ProfileForm({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    String appTitle = "Profile form";
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text(appTitle),
              backgroundColor: Colors.orange,
            ),
            body: Column(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Row(
                      children: <Widget>[
                        Flexible(
                            flex: 1,
                            child: Container(
                              color: Colors.amberAccent,
                            )),
                        Flexible(
                            flex: 1,
                            child: Container(
                              color: Colors.yellow,
                            )),
                        Flexible(
                            flex: 1,
                            child: Container(
                              color: Colors.green,
                            ))
                      ],
                    )),
                Flexible(
                    flex: 3,
                    child: Container(
                      color: Colors.blue,
                    )),
                Flexible(
                    flex: 1,
                    child: Container(
                      color: Colors.green,
                    ))
              ],
            )));
  }
}

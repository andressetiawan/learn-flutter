import 'package:flutter/material.dart';

class LayoutStack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body: Stack(
          children: [
            const CircleAvatar(
              radius: 100,
            ),
            Container(
              margin: EdgeInsets.only(top: 50, left: 50),
              width: 100,
              height: 100,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(width: 4, color: Colors.white),
                  borderRadius: BorderRadius.all(Radius.circular(25))),
              child: const Text(
                'Click',
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

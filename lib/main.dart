import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:learn_flutter/components/ColumnRow.dart';
import 'package:learn_flutter/components/CounterApp.dart';
import 'package:learn_flutter/components/FormPage.dart';
import 'package:learn_flutter/components/LayoutGridView.dart';
import 'package:learn_flutter/components/LayoutStack.dart';
import 'package:learn_flutter/components/ListViewPage.dart';
import 'package:learn_flutter/components/ProfileForm.dart';
// import 'package:learn_flutter/components/CounterApp.dart';
// import 'package:learn_flutter/components/LatStatefulWidget.dart';
import 'package:learn_flutter/components/LayoutGridExtent.dart';
// import 'package:learn_flutter/components/LayoutGridView.dart';
import 'package:learn_flutter/components/LayoutListView.dart';
import 'package:learn_flutter/components/TodoApps.dart';
import 'package:learn_flutter/components/TrainingNavigation.dart';
import 'package:learn_flutter/components/UICaseStudy/LoginForm.dart';
// import 'package:learn_flutter/components/LayoutStack.dart';
// import 'package:learn_flutter/components/ManageByParent.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
  runApp(LoginForm());
}
